import os
import shutil

import pytest

from storage.storage import get_path


@pytest.fixture
def paths():
    relative_dir = os.path.dirname(os.path.abspath(__file__))
    reservoir = 'reservoir'
    yield relative_dir, reservoir
    shutil.rmtree(os.path.join(relative_dir, reservoir))


def test_get_path(mocker, paths):
    relative_dir, reservoir = paths

    def mock_relative_filedir(*args):
        return relative_dir

    from importlib import import_module
    module = import_module('storage.storage')

    mocker.patch.object(module, '_relative_filedir', mock_relative_filedir)
    mocker.patch.object(module, 'RESERVOIR_PATH', reservoir)

    path = get_path()
    assert path == os.path.join(relative_dir, reservoir)

    folder = 'abc/def'
    path = get_path(path=folder)
    assert path == os.path.join(relative_dir, reservoir, folder)

    filename = 'filename.fls'
    path = get_path(filename=filename)
    assert path == os.path.join(relative_dir, reservoir, filename)

    folder = 'abc/def'
    filename = 'filename.fls'
    path = get_path(folder, filename)
    assert path == os.path.join(relative_dir, reservoir, folder, filename)
