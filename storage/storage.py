import os

RESERVOIR_PATH = 'reservoir'


def _relative_filedir():
    return os.path.dirname(os.path.abspath(__file__))


def get_path(path=None, filename=None):
    '''
    Determine the absolute path of a filename relative to a reservoir.

    :param path:
    Path inside the reservoir.

    :param filename:
    Filename to get the absolute path.

    :return:
    The absolute path of the filename in the system.
    '''

    abs_path = os.path.join(_relative_filedir(), RESERVOIR_PATH)

    if path:
        abs_path = os.path.join(abs_path, path)
        if not os.path.exists(abs_path):
            os.makedirs(abs_path)

    if filename:
        abs_path = os.path.join(abs_path, filename)

    return abs_path
