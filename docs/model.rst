model package
=============

Submodules
----------

model.experiment module
-----------------------

.. automodule:: model.experiment
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: model
   :members:
   :undoc-members:
   :show-inheritance:
