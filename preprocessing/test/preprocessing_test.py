import pandas as pd

from preprocessing.preprocessing import oversampling_data


def test_oversampling_data():
    x_data = {
        'x1': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        'x2': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    }
    y_data = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1]

    x = pd.DataFrame.from_dict(x_data)
    y = pd.Series(y_data)

    assert x['x1'].size == 10
    assert x['x2'].size == 10
    assert y.size == 10
    assert y[y == 0].size == 8
    assert y[y == 1].size == 2

    x_over, y_over = oversampling_data(x, y)

    assert x_over['x1'].size == 16
    assert x_over['x2'].size == 16
    assert y_over.size == 16
    assert y_over[y_over == 0].size == 8
    assert y_over[y_over == 1].size == 8
