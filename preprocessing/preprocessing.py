import pandas as pd


def oversampling_data(data_x, data_y):
    '''
    Apply a random oversampling strategy in the attribute and target data.

    :param data_x:
    The attributes data to be oversampled.

    :param data_y:
    The target data to be oversampled.

    :return pd.DataFrame:
    The attributes data oversampled.

    :return pd.Series:
    The target data oversampled.
    '''

    from imblearn.over_sampling import RandomOverSampler

    random_sampler = RandomOverSampler(random_state=50)
    x_oversampled, y_oversampled = random_sampler.fit_resample(data_x, data_y)

    x_oversampled_df = pd.DataFrame(x_oversampled, columns=data_x.columns)
    y_oversampled_sr = pd.Series(y_oversampled)

    return x_oversampled_df, y_oversampled_sr
