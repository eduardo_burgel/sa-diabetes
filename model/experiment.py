import pandas as pd
from joblib import dump
from sklearn.ensemble import RandomForestClassifier

from preprocessing.preprocessing import oversampling_data
from storage.storage import get_path

DATA_PATH = 'data'

DATA_FILENAME = 'data.csv'

MODEL_PATH = 'model'

MODEL_FILENAME = 'model.joblib'


def dump_model():
    '''
    Persist a fitted model to be used in the service prediction.
    '''

    # Load data.
    df = pd.read_csv(get_path(DATA_PATH, DATA_FILENAME))

    selected_features = ['Body Mass Index', 'Glucose', 'Hemoglobin A1c/Hemoglobin.total in Blood', 'Diagnosis']
    df = df[selected_features]

    # Remove data with invalid values (NaN).
    df = df.dropna()

    attributes = ['Body Mass Index', 'Glucose', 'Hemoglobin A1c/Hemoglobin.total in Blood']
    target = 'Diagnosis'

    x = df[attributes]
    y = df[target]

    x, y = oversampling_data(x, y)

    # Fit model.
    model = RandomForestClassifier(random_state=50)
    model = model.fit(x, y)

    # Dump model.
    dump(model, get_path(MODEL_PATH, MODEL_FILENAME))
