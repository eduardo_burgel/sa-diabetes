import os

from model.experiment import dump_model, MODEL_PATH, MODEL_FILENAME
from storage.storage import get_path


def test_persist_model():
    dump_model()

    assert os.path.isfile(get_path(MODEL_PATH, MODEL_FILENAME))
