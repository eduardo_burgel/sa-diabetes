from invoke import task


@task()
def notebook(context):
    context.run('jupyter notebook --notebook-dir=\'notebook\'')


@task()
def docs(context):
    # How to initialize the docs folder: sphinx-quickstart --ext-autodoc.
    context.run('sphinx-apidoc -o docs/ model/')
    context.run('sphinx-apidoc -o docs/ preprocessing/')
    context.run('sphinx-apidoc -o docs/ service/')
    context.run('sphinx-apidoc -o docs/ storage/')
    context.run('sphinx-build -b html docs/ docs/html/')


@task()
def test(context):
    context.run('python -m  pytest -vv -p no:cacheprovider --ignore=service/integration_test/ .')


@task(test)
def build(context):
    context.run('rm -rf model/model.joblib')
    context.run('python -c \'from model.experiment import dump_model; dump_model();\'')


@task(build)
def deploy(context):
    context.run('docker-compose build diabetes_service')
    context.run('docker system prune -f')


@task()
def start(context):
    context.run('docker-compose up -d')


@task()
def stop(context):
    context.run('docker-compose down')


@task(pre=[start], post=[stop])
def integration(context):
    context.run('python -m pytest -vv -p no:cacheprovider service/integration_test/')


@task(pre=[start], post=[stop])
def performance(context):
    # The address of the locust web interface (http://localhost:8089).
    context.run('locust -f locustfile.py -H http://localhost:5000')
