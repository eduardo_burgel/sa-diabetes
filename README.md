# Diabetes Service
The diabetes service consists in a machine learning model that will classify if a patient has a normal, prediabetes or 
diabetes diagnosis based in some attributes.
The model is deployed as a microservice, a REST API that accepts requests containing a single patient attributes as a 
JSON and returns a response containing the model prediction (the patient diabetes diagnosis).

## Project and Environment
Clone the project, create the conda environment and activate it.  
Dependencies: you need **git** and **conda** installed in your environment.
```sh
git clone https://bitbucket.org/eduardo_burgel/sa-diabetes.git

cd sa-diabetes

conda env create -f environment.yml

source activate sa-diabetes
```

## Tasks
Once your conda environment is configured and activated, you can run invoke to perform the required tasks.  
Dependencies: you need **docker** and **docker-compose** installed in your environment.  
To see all the available tasks:
```sh
invoke --list

Available tasks:
  test
  build
  deploy
  -
  start
  stop
  integration
  performance
  -
  docs
  notebook
```
 
The tasks are grouped by objectives and can be used as in the example:
```sh
invoke {task_name}
```
---------------------------------------
The **test** task will run all unit tests.

The **build** task will generate the diabetes diagnosis model.  

The **deploy** task will build and deploy the model in a docker image.
¯
Calling deploy you will run all the unit tests, build the model and create the diabetes service docker image.
```sh
invoke deploy
```

---------------------------------------
  
The **start** task will start a diabetes service docker container.
  
The **stop** task will stop a running diabetes service docker container.
  
The **integration** task will run the integration tests.
  
The **performance** task will start the diabetes service locally, out of a docker container.

Calling integration or performance will automatically start a diabetes service container and stop this container after 
the action.
```sh
invoke integration
```

To assert that diabetes service is running and performing correctly, you can call the integration task.

If you would like to evaluate the performance of the diabetes service you can call the performance task.  
It will provide a web interface at http://localhost:8089 where you can configure the load test to evaluate the service.
```sh
invoke performance
```

---------------------------------------

The **docs** task will build the documentation.  
You can find this in docs/html folder and open the index.html.
```sh
invoke docs
```

The **notebook** task will start a jupyter instance.  
In the jupyter you can consult the analysis notebook with considerations about the diabetes service model.  
```sh
invoke notebook
```
