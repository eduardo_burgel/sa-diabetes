import pytest
import requests


def test_churn_service_sucess():
    request_data = {
        'Body Mass Index': 29.142857,
        'Glucose': 101.516660,
        'Hemoglobin A1c/Hemoglobin.total in Blood': 6.871429,
    }

    response = requests.post('http://localhost:5000/diabetes_service', json=request_data)

    # Append diabetes diagnosis result in request data for comparison with response data.
    request_data['Diagnosis'] = 2

    assert response.status_code == 200
    assert response.json() == request_data


@pytest.mark.parametrize(
    'request_data, http_code, error_message',
    [
        # Data with empty data.
        (
            {},
            400,
            'Empty attribute data.',
        ),
        # Incomplete data.
        (
            {
                'Body Mass Index': 0,
                'Glucose': 0,
            },
            400,
            'Missing attribute(s): [\'Hemoglobin A1c/Hemoglobin.total in Blood\'].',
        ),
        # Wrong attribute type.
        (
            {
                'Body Mass Index': 'abcde',
                'Glucose': 0,
                'Hemoglobin A1c/Hemoglobin.total in Blood': 0,
            },
            400,
            'Invalid feature data.',
        ),
    ],
)
def test_diabetes_service_error(request_data, http_code, error_message):
    response = requests.post('http://localhost:5000/diabetes_service', json=request_data)

    # Append error message in request data for comparison with error response data.
    request_data['Error'] = error_message

    assert response.status_code == http_code
    assert response.json() == request_data
