import pandas as pd
from flask import Flask, request
from flask_restful import Api, Resource
from joblib import load

from model.experiment import MODEL_PATH, MODEL_FILENAME
from storage.storage import get_path


class DiabetesService(Resource):
    '''
    Implements rest service api for diabetes service.
    '''

    def __init__(self):
        self._model = load(get_path(MODEL_PATH, MODEL_FILENAME))

    def post(self):
        '''
        Handle the post http method for the customer diabetes service.

        :return list:
        The diabetes prediction.

        :return int:
        The http code status of the response.
        '''

        data = request.get_json()

        if not data:
            data['Error'] = 'Empty attribute data.'
            return data, 400

        attributes = [
            'Body Mass Index',
            'Glucose',
            'Hemoglobin A1c/Hemoglobin.total in Blood',
        ]

        missing_attributes = [key for key in sorted(attributes) if key not in data.keys()]
        if missing_attributes:
            data['Error'] = 'Missing attribute(s): %s.' % missing_attributes
            return data, 400

        # Select only the attributes required by the model.
        selected_data = {key: value for key, value in data.items() if key in attributes}

        try:
            customer = pd.DataFrame(selected_data, index=[0])

            # Make prediction.
            churn_prediction = self._model.predict(customer)
        except Exception:
            data['Error'] = 'Invalid feature data.'
            return data, 400

        data['Diagnosis'] = churn_prediction.tolist()[0]
        return data, 200


app = Flask(__name__)
api = Api(app)
api.add_resource(DiabetesService, '/diabetes_service')
