from locust import HttpUser, TaskSet, task


class DiabetesServiceTasks(TaskSet):
    '''
    Describe tasks executed during performance evaluation.
    '''

    @task(1)
    def call_service(self):
        request_data = {
            'Body Mass Index': 29.142857,
            'Glucose': 101.516660,
            'Hemoglobin A1c/Hemoglobin.total in Blood': 6.871429,
        }

        headers = {'content-type': 'application/json'}

        self.client.post('http://localhost:5000/diabetes_service', json=request_data, headers=headers)


class WebsiteUser(HttpUser):
    '''
    Describe user behaviour during performance evaluation.
    '''

    tasks = [DiabetesServiceTasks]

    min_wait = 1000
    max_wait = 2000
