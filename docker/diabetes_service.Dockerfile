FROM continuumio/miniconda3:4.9.2

MAINTAINER DIABETES_SERVICE "diabetes_service@sa-health.com.br"

WORKDIR /home/diabetes_service

ADD docker/diabetes_service_environment.yml /home/diabetes_service/environment.yml
RUN conda update -n base conda && \
    conda env create -f environment.yml && \
    conda clean --all -y

ADD docker/diabetes_service_start.sh /home/diabetes_service/start.sh

ADD model /home/diabetes_service/model
ADD preprocessing /home/diabetes_service/preprocessing
ADD service /home/diabetes_service/service
ADD storage /home/diabetes_service/storage

ENV PATH="${PATH}:/home/diabetes_service"

EXPOSE 5000

ENTRYPOINT ["start.sh"]
