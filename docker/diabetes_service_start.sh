#!/bin/bash

function activate_environment {
    source activate diabetes-service
}

function start_service {
    gunicorn -w 4 -b 0.0.0.0:5000 service.service:app
}

activate_environment
start_service
